
## Still left to do

- Much more testing... specifically, testing related to the rendering
of React components using Enzyme
- Add flow type annotations throughout project codebase and in my
redux-most middleware library (currently only has TypeScript definitions)
