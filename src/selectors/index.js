// @flow
import { createSelector } from 'reselect'
import {
  APP,
  IS_FETCHING_PRODUCTS,
  IS_FETCHING_SCHEMA,
  IS_PINGING,
  PING_PONG_EXAMPLE,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
  TITLE,
} from '../constants/stateKeys'
import compose from 'ramda/src/compose'
import map from 'ramda/src/map'
import prop from 'ramda/src/prop'
import uniq from 'ramda/src/uniq'
import values from 'ramda/src/values'

export const getApp = prop(APP)
export const getPingPongExample = prop(PING_PONG_EXAMPLE)

// eliminate boilerplate for simple single prop get selectors
const createGetSelector = f => x => createSelector(
  [f],
  prop(x)
)

// use currying to create reusable functions only needing a prop arg
const appSelect = createGetSelector(getApp)
const pingPongSelect = createGetSelector(getPingPongExample)

// simple get selectors
export const getAppTitle = appSelect(TITLE)
export const getIsFetchingProducts = appSelect(IS_FETCHING_PRODUCTS)
export const getProductsLoaded = appSelect(PRODUCTS_LOADED)
export const getIsFetchingSchema = appSelect(IS_FETCHING_SCHEMA)
export const getSchemaLoaded = appSelect(SCHEMA_LOADED)
export const getIsPinging = pingPongSelect(IS_PINGING)
export const getSelectedProduct = appSelect(SELECTED_PRODUCT)

// other selectors
export const getProducts = createSelector(
  [getApp],
  compose(
    values,
    prop(PRODUCTS)
  )
)

export const getProductByID = productId => createSelector(
  [getApp],
  app => compose(
    prop(productId),
    prop(PRODUCTS)
  )(app) || {}
)

export const getSchema = createSelector(
  [getApp],
  compose(values, prop(SCHEMA))
)

export const getSchemaFieldByKey = fieldKey => createSelector(
  [getApp],
  app => compose(
    prop(fieldKey),
    prop(SCHEMA)
  )(app) || {}
)

export const getStatusOptions = createSelector(
  [getApp],
  compose(
    uniq,
    map(prop('status')),
    values,
    prop(PRODUCTS)
  )
)
