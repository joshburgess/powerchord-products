// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import { formatCurrency } from '../../utils'

const ProductListItem = ({
  ID,
  status,
  upc,
  name,
  price,
  description,
  setSelectedProduct,
}) =>
  <Link
    key={ID}
    to={`/${ID}`}
    onClick={() => setSelectedProduct(ID)}
  >
    <div className='product-list-item'>
      <h3 className='name'>{name}</h3>
      <h5 className='upc'>{upc}</h5>
      <h4 className='price'>{formatCurrency(price)}</h4>
    </div>
  </Link>

export default ProductListItem
