// @flow
import React from 'react'
import { formatCurrency } from '../../utils'
import map from 'ramda/src/map'
import prop from 'ramda/src/prop'

const PresentationWrapper = ({ input, fieldId, label }) =>
  <div className='field'>
    <label htmlFor={fieldId}>{label}</label>
    {input}
  </div>

export const CurrencyPresentation = ({ fieldId, label, value }) => {
  const input = <input type='text' value={formatCurrency(value)} readOnly />

  return <PresentationWrapper fieldId={fieldId} input={input} label={label} />
}

export const NumberPresentation = ({ fieldId, label, value }) => {
  const input = <input type='number' value={value} readOnly />

  return <PresentationWrapper fieldId={fieldId} input={input} label={label} />
}

export const SelectPresentation = ({ fieldId, label, options, value }) => {
  const noOptionsText = 'Error: No options found'
  const input =
    <select id={fieldId} value={value} readOnly>
      {!options
        ? <option value={noOptionsText}>{noOptionsText}</option>
        : map(x =>
          <option key={x} value={x}>{x}</option>
        )(options)
      }
    </select>

  return <PresentationWrapper fieldId={fieldId} input={input} label={label} />
}

export const TextPresentation = ({ fieldId, label, value }) => {
  const input = value && prop('length')(value) > 60
    ? <textarea value={value} readOnly />
    : <input type='text' value={value} readOnly />

  return <PresentationWrapper fieldId={fieldId} input={input} label={label} />
}
