// @flow
export const DESCRIPTION = 'description'
export const ID = 'ID'
export const NAME = 'name'
export const PRICE = 'price'
export const STATUS = 'status'
export const UPC = 'upc'
