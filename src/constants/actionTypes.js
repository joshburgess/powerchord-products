// @flow
// ping pong example
export const PING = 'PING'
export const PONG = 'PONG'

// main app
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS'
export const FETCH_SCHEMA = 'FETCH_SCHEMA'
export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS'
export const RECEIVE_SCHEMA = 'RECEIVE_SCHEMA'
export const SET_IS_FETCHING_PRODUCTS = 'SET_IS_FETCHING_PRODUCTS'
export const SET_IS_FETCHING_SCHEMA = 'SET_IS_FETCHING_SCHEMA'
export const SET_SELECTED_PRODUCT = 'SET_SELECTED_PRODUCT'
export const SET_PRODUCTS_LOADED = 'SET_PRODUCTS_LOADED'
export const SET_SCHEMA_LOADED = 'SET_SCHEMA_LOADED'
