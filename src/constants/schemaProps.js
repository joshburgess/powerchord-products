// @flow
export const LABEL = 'label'
export const KEY = 'key'
export const POSITION = 'position'
export const PRESENTATION = 'presentation'
