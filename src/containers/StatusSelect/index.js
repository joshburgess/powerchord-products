// @flow
import { connect } from 'react-redux'
import { getStatusOptions } from '../../selectors'
import { SelectPresentation } from '../../components/Presentations'

const mapStateToProps = state => ({
  options: getStatusOptions(state),
})

export default connect(mapStateToProps, {})(SelectPresentation)
