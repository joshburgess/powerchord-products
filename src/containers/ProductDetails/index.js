// @flow
import React from 'react'
import { connect } from 'react-redux'
import {
  getSelectedProduct,
  getProductByID,
  getProductsLoaded,
  getSchemaFieldByKey,
  getSchemaLoaded,
} from '../../selectors'
import {
  setSelectedProduct,
} from '../../actions'
import ErrorMessage from '../../components/ErrorMessage'
import LoadingMessage from '../../components/LoadingMessage'
import compose from 'ramda/src/compose'
import isEmpty from 'ramda/src/isEmpty'
import map from 'ramda/src/map'
import prop from 'ramda/src/prop'
import sortBy from 'ramda/src/sortBy'
import { getPresentation } from '../../utils/getPresentation'
// I would setup webpack 2 or rollup to use tree shaking or use
// the babel lodash plugin set to work with recompose to avoid
// importing the entire library in a production scenario
import { lifecycle } from 'recompose'

const withSetSelectedProductSafeguard = lifecycle({
  /* eslint-disable fp/no-this */
  /* eslint-disable fp/no-nil */
  /* eslint-disable fp/no-unused-expression */
  /* eslint-disable better/explicit-return */
  /* eslint-disable better/no-ifs */
  componentWillReceiveProps () {
    const {
      selectedProduct,
      match,
      setSelectedProduct,
      productsLoaded,
    } = this.props

    const { params: { id } } = match

    // I prefer avoiding imperative control flow like this,
    // but React's lifecycle & class API somewhat necessitates it
    if (productsLoaded && !selectedProduct && id) {
      setSelectedProduct(id)
    }
    /* eslint-enable better/no-ifs */
    /* eslint-enable better/explicit-return */
    /* eslint-enable fp/no-unused-expression */
    /* eslint-enable fp/no-nil */
    /* eslint-enable fp/no-this */
  },
})

const ProductDetails = withSetSelectedProductSafeguard(({
  selectedProduct,
  match,
  setSelectedProduct,
  productsLoaded,
  productDetailFields,
  schemaLoaded,
}) =>
  !productsLoaded || !schemaLoaded
    ? <LoadingMessage />
    : (!selectedProduct || !productDetailFields || isEmpty(productDetailFields)
      ? <ErrorMessage />
      : (
        <div className='App-intro-strip'>
          <h3 className='App-intro'>
            Product Details
          </h3>
          <div className='product-details'>
            {
              map(({ key, value, label, presentation }) => {
                const Presentation = getPresentation(presentation)
                return (
                  <Presentation
                    key={key}
                    fieldId={`${selectedProduct}_${key}`}
                    label={label}
                    value={value}
                  />
                )
              })(productDetailFields)
            }
          </div>
        </div>
      )
    )
)

const mapStateToProps = state => {
  const selectedProduct = getSelectedProduct(state)
  const product = getProductByID(selectedProduct)(state)

  const toProductDetailField = ([key, val]) => ({
    value: val,
    ...getSchemaFieldByKey(key)(state),
  })

  const getProductDetailFields = compose(
    sortBy(prop('position')),
    map(toProductDetailField),
    Object.entries
  )

  const productDetailFields = !product || isEmpty(product)
    ? []
    : getProductDetailFields(product)

  return {
    selectedProduct,
    productDetailFields,
    productsLoaded: getProductsLoaded(state),
    schemaLoaded: getSchemaLoaded(state),
  }
}

const mapDispatchToProps = {
  setSelectedProduct,
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails)
