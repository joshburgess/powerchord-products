// @flow
import React from 'react'
import { connect } from 'react-redux'
import {
  getAppTitle,
  getProducts,
  getProductsLoaded,
} from '../../selectors'
import { setSelectedProduct } from '../../actions'
import map from 'ramda/src/map'
import ErrorMessage from '../../components/ErrorMessage'
import LoadingMessage from '../../components/LoadingMessage'
import ProductListItem from '../../components/ProductListItem'
import prop from 'ramda/src/prop'
import { ID } from '../../constants/schemaKeys'

const Products = ({ products, productsLoaded, title, setSelectedProduct }) =>
  !productsLoaded
    ? <LoadingMessage />
    : (!products
      ? <ErrorMessage />
      : (
        <div className='App-intro-strip'>
          <h3 className='App-intro'>
            Click on a product to view more details
          </h3>
          <div>
            {
              map(props =>
                <ProductListItem
                  {...props}
                  key={prop(ID)(props)}
                  setSelectedProduct={setSelectedProduct}
                />
              )(products)
            }
          </div>
        </div>
      )
    )

const mapStateToProps = state => ({
  title: getAppTitle(state),
  products: getProducts(state),
  productsLoaded: getProductsLoaded(state),
})

const mapDispatchToProps = {
  setSelectedProduct,
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)
