// @flow
import React from 'react'
import { Provider } from 'react-redux'
import './App.css'
import history from './history'
import { Route } from 'react-router'
import { ConnectedRouter } from 'react-router-redux'
import Header from './containers/Header'
import Products from './containers/Products'
import ProductDetails from './containers/ProductDetails'
import logo from './logo.png'

const App = ({ store }) =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className='App'>
        <Header logo={logo} />
        <Route exact path='/' component={Products} />
        <Route exact path='/:id' component={ProductDetails} />
      </div>
    </ConnectedRouter>
  </Provider>

export default App
