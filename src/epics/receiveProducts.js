// @flow
import { select } from 'redux-most'
import compose from 'ramda/src/compose'
import {
  curriedSwitchMap as switchMap,
  curriedMerge as merge,
} from '../utils'
import { just } from 'most'
import { RECEIVE_PRODUCTS } from '../constants/actionTypes'
import {
  setIsFetchingProducts,
  setProductsLoaded,
} from '../actions'

const afterReceivedProducts = () => merge(
  just(setIsFetchingProducts(false)),
  just(setProductsLoaded(true))
)

const receiveFilteredResults = compose(
  switchMap(afterReceivedProducts),
  select(RECEIVE_PRODUCTS),
)

export default receiveFilteredResults
