// @flow
import { select } from 'redux-most'
import compose from 'ramda/src/compose'
import {
  curriedMap as map,
  curriedMerge as merge,
  curriedSwitchMap as switchMap,
} from '../utils'
import { getProductsStream } from '../utils/getProductsStream'
import { just } from 'most'
import { FETCH_PRODUCTS } from '../constants/actionTypes'
import {
  setIsFetchingProducts,
  receiveProducts,
} from '../actions'

const getReceivedProducts = () => merge(
  just(setIsFetchingProducts(true)),
  map(receiveProducts)(getProductsStream())
)

const fetchProducts = compose(
  switchMap(getReceivedProducts),
  select(FETCH_PRODUCTS)
)

export default fetchProducts
