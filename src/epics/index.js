// @flow
import { combineEpics } from 'redux-most'
import pingPongExample from './pingPongExample'
import fetchProducts from './fetchProducts'
import fetchSchema from './fetchSchema'
import receiveProducts from './receiveProducts'
import receiveSchema from './receiveSchema'

const rootEpic = combineEpics([
  pingPongExample,
  fetchProducts,
  fetchSchema,
  receiveProducts,
  receiveSchema,
])

export default rootEpic
