// @flow
import { select } from 'redux-most'
import compose from 'ramda/src/compose'
import {
  curriedSwitchMap as switchMap,
  curriedMerge as merge,
} from '../utils'
import { just } from 'most'
import { RECEIVE_SCHEMA } from '../constants/actionTypes'
import {
  setIsFetchingSchema,
  setSchemaLoaded,
} from '../actions'

const afterReceivedSchema = () => merge(
  just(setIsFetchingSchema(false)),
  just(setSchemaLoaded(true)),
)

const receiveFilteredResults = compose(
  switchMap(afterReceivedSchema),
  select(RECEIVE_SCHEMA),
)

export default receiveFilteredResults
