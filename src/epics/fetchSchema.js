// @flow
import { select } from 'redux-most'
import compose from 'ramda/src/compose'
import {
  curriedMap as map,
  curriedMerge as merge,
  curriedSwitchMap as switchMap,
} from '../utils'
import { getSchemaStream } from '../utils/getSchemaStream'
import { just } from 'most'
import { FETCH_SCHEMA } from '../constants/actionTypes'
import {
  setIsFetchingSchema,
  receiveSchema,
} from '../actions'

const getReceivedSchema = () => merge(
  just(setIsFetchingSchema(true)),
  map(receiveSchema)(getSchemaStream())
)

const fetchSchema = compose(
  switchMap(getReceivedSchema),
  select(FETCH_SCHEMA)
)

export default fetchSchema
