import test from 'ava'
import appReducer from './app'
// import {
//   RECEIVE_PRODUCTS,
//   RECEIVE_SCHEMA,
//   SET_PRODUCTS_LOADED,
//   SET_SCHEMA_LOADED,
//   SET_SELECTED_PRODUCT,
//   SET_IS_FETCHING_PRODUCTS,
//   SET_IS_FETCHING_SCHEMA,
// } from '../constants/actionTypes'
import {
  IS_FETCHING_PRODUCTS,
  IS_FETCHING_SCHEMA,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
  TITLE,
} from '../constants/stateKeys'
import {
  receiveProducts,
  receiveSchema,
  setProductsLoaded,
  setSchemaLoaded,
  setSelectedProduct,
  setIsFetchingProducts,
  setIsFetchingSchema,
} from '../actions'
import merge from 'ramda/src/merge'
/* eslint-disable */

const initialState = {
  [TITLE]: 'PowerChord Products Exercise',
  [IS_FETCHING_PRODUCTS]: false,
  [IS_FETCHING_SCHEMA]: false,
  [PRODUCTS]: [],
  [PRODUCTS_LOADED]: false,
  [SCHEMA]: [],
  [SCHEMA_LOADED]: false,
  [SELECTED_PRODUCT]: '',
}

const mockProducts = [
  {
    ID: 1,
    status: 'scheduled',
    upc: 'BA776009705700629004',
    name: 'Handcrafted Rubber Pizza',
    price: '283.63',
    description:
      'Unde voluptatem nihil. Repellendus est ab. Rerum nam nemo dignissimos quia corporis expedita. Debitis omnis enim corporis et assumenda.',
  },
  {
    ID: 2,
    status: 'scheduled',
    upc: 'PT56099090506183082764604',
    name: 'Practical Wooden Chicken',
    price: '167.43',
    description:
      'Corrupti eaque fuga consequatur officia illum et rem excepturi. Sunt non et consequatur adipisci sit minima magnam saepe sunt. Aspernatur facere mollitia laboriosam deserunt reiciendis.',
  },
]

const expectedProducts = {
  '1': {
    ID: 1,
    status: 'scheduled',
    upc: 'BA776009705700629004',
    name: 'Handcrafted Rubber Pizza',
    price: '283.63',
    description: 'Unde voluptatem nihil. Repellendus est ab. Rerum nam nemo dignissimos quia corporis expedita. Debitis omnis enim corporis et assumenda.'
  },
  '2': {
    ID: 2,
    status: 'scheduled',
    upc: 'PT56099090506183082764604',
    name: 'Practical Wooden Chicken',
    price: '167.43',
    description: 'Corrupti eaque fuga consequatur officia illum et rem excepturi. Sunt non et consequatur adipisci sit minima magnam saepe sunt. Aspernatur facere mollitia laboriosam deserunt reiciendis.'
  },
}

const mockSchema = {
  resource: 'products',
  fields: [
    {
      label: 'ID',
      key: 'ID',
      position: 0,
      presentation: 'number',
    },
    {
      label: 'Status',
      key: 'status',
      position: 6,
      presentation: 'select',
    },
    {
      label: 'Name',
      key: 'name',
      position: 1,
      presentation: 'text',
    },
  ]
}

const expectedSchema = {
  ID: {
    label: 'ID',
    key: 'ID',
    position: 0,
    presentation: 'number',
  },
  status: {
    label: 'Status',
    key: 'status',
    position: 6,
    presentation: 'select',
  },
  name: {
    label: 'Name',
    key: 'name',
    position: 1,
    presentation: 'text',
  },

}

test('appReducer should default to returning the initial state', t => {
  const result = appReducer(undefined, {})
  t.deepEqual(initialState, result)
})

test('appReducer should handle RECEIVE_PRODUCTS actions', t => {
  const result = appReducer(initialState, receiveProducts(mockProducts))
  const expected = merge(initialState)({
    [PRODUCTS]: expectedProducts,
  })
  t.deepEqual(expected, result)
})

test('appReducer should handle RECEIVE_SCHEMA actions', t => {
  const result = appReducer(initialState, receiveSchema(mockSchema))
  const expected = merge(initialState)({
    [SCHEMA]: expectedSchema,
  })
  t.deepEqual(expected, result)
})

test('appReducer should handle SET_PRODUCTS_LOADED actions', t => {
  const result = appReducer(initialState, setProductsLoaded(true))
  const expected = merge(initialState)({
    [PRODUCTS_LOADED]: true,
  })
  t.deepEqual(expected, result)
})

test('appReducer should handle SET_SCHEMA_LOADED actions', t => {
  const result = appReducer(initialState, setSchemaLoaded(true))
  const expected = merge(initialState)({
    [SCHEMA_LOADED]: true,
  })
  t.deepEqual(expected, result)
})

test('appReducer should handle SET_SELECTED_PRODUCT actions', t => {
  const result = appReducer(initialState, setSelectedProduct(1))
  const expected = merge(initialState)({
    [SELECTED_PRODUCT]: 1,
  })
  t.deepEqual(expected, result)
})

test('appReducer should handle SET_IS_FETCHING_PRODUCTS actions', t => {
  const result = appReducer(initialState, setIsFetchingProducts(true))
  const expected = merge(initialState)({
    [IS_FETCHING_PRODUCTS]: true,
  })
  t.deepEqual(expected, result)
})

test('appReducer should handle SET_IS_FETCHING_SCHEMA actions', t => {
  const result = appReducer(initialState, setIsFetchingSchema(true))
  const expected = merge(initialState)({
    [IS_FETCHING_SCHEMA]: true,
  })
  t.deepEqual(expected, result)
})



