// @flow
import {
  RECEIVE_PRODUCTS,
  RECEIVE_SCHEMA,
  SET_PRODUCTS_LOADED,
  SET_SCHEMA_LOADED,
  SET_SELECTED_PRODUCT,
  SET_IS_FETCHING_PRODUCTS,
  SET_IS_FETCHING_SCHEMA,
} from '../constants/actionTypes'
import {
  IS_FETCHING_PRODUCTS,
  IS_FETCHING_SCHEMA,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
  TITLE,
} from '../constants/stateKeys'
import {
  nextState,
  mergeSetIsFetchingProducts,
  mergeSetIsFetchingSchema,
  mergeSetProductsLoaded,
  mergeSetSchemaLoaded,
  mergeReceiveProducts,
  mergeReceiveSchema,
  mergeSetSelectedProduct,
} from '../utils/reducer-utils'

const initialState = {
  [TITLE]: 'PowerChord Products Exercise',
  [IS_FETCHING_PRODUCTS]: false,
  [IS_FETCHING_SCHEMA]: false,
  [PRODUCTS]: [],
  [PRODUCTS_LOADED]: false,
  [SCHEMA]: [],
  [SCHEMA_LOADED]: false,
  [SELECTED_PRODUCT]: '',
}

const app = (state = initialState, { type, payload }) => {
  const updates = {
    [RECEIVE_PRODUCTS]: () => mergeReceiveProducts(state)(payload),
    [RECEIVE_SCHEMA]: () => mergeReceiveSchema(state)(payload),
    [SET_IS_FETCHING_PRODUCTS]: () => mergeSetIsFetchingProducts(state)(payload),
    [SET_IS_FETCHING_SCHEMA]: () => mergeSetIsFetchingSchema(state)(payload),
    [SET_PRODUCTS_LOADED]: () => mergeSetProductsLoaded(state)(payload),
    [SET_SCHEMA_LOADED]: () => mergeSetSchemaLoaded(state)(payload),
    [SET_SELECTED_PRODUCT]: () => mergeSetSelectedProduct(state)(payload),
  }

  return nextState(state)(updates)(type)
}

export default app
