import test from 'ava'
import {
  getSchemaStream,
} from './getSchemaStream'
import {
  PRODUCT_SCHEMA,
} from '../constants/product_schema'
import {
  curriedMap as map,
  curriedTake as take,
} from '../utils'
import compose from 'ramda/src/compose'
import isObservable from 'is-observable'

/* eslint-disable */

test('getSchemaStream returns an observable', t => { 
  const value = isObservable(getSchemaStream())
  t.true(value)
})

test('the observable returned by getSchemaStream emits the product schema object', t => { 
  // ava features direct support for Observables! 
  const stream = compose(
    map(x => PRODUCT_SCHEMA === x ? t.pass() : t.fail()),
    (take)(1),
    getSchemaStream
  )()
})