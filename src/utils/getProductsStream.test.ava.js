import test from 'ava'
import {
  getProductsStream,
} from './getProductsStream'
import {
  PRODUCT_DATA,
} from '../constants/product_data'
import {
  curriedMap as map,
  curriedTake as take,
} from '../utils'
import compose from 'ramda/src/compose'
import isObservable from 'is-observable'

/* eslint-disable */

test('getProductsStream returns an observable', t => {
  const value = isObservable(getProductsStream())
  t.true(value)
})

test('the observable returned by getProductsStream emits the product data object', t => {
  // ava features direct support for Observables! 
  return compose(
    map(x => PRODUCT_DATA === x ? t.pass() : t.fail()),
    (take)(1),
    getProductsStream
  )()
})