// @flow
import { getFakeResponseStream } from './'
import { PRODUCT_DATA } from '../constants/product_data'

export const getProductsStream = () => getFakeResponseStream(PRODUCT_DATA)
