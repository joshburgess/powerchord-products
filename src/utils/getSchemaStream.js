// @flow
import { getFakeResponseStream } from './'
import { PRODUCT_SCHEMA } from '../constants/product_schema'

export const getSchemaStream = () => getFakeResponseStream(PRODUCT_SCHEMA)
