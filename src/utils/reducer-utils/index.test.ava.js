import test from 'ava'
import {
  nextState,
} from './'
import {
  IS_FETCHING_PRODUCTS,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
  TITLE,
} from '../../constants/stateKeys'
import merge from 'ramda/src/merge'

/* eslint-disable */

const mockState = {
  app: {
    [TITLE]: 'PowerChord Products Exercise',
    [IS_FETCHING_PRODUCTS]: false,
    [PRODUCTS]: [],
    [PRODUCTS_LOADED]: false,
    [SCHEMA]: [],
    [SCHEMA_LOADED]: false,
    [SELECTED_PRODUCT]: '',
  },
}

test('nextState returns the previous state when an update function is not found', t => {
  const actionType = 'SET_SUBTITLE'
  const mockPayload = { subtitle:'New Subtitle' }
  const mockUpdates = {
    SET_TITLE: () => merge(mockState)(mockPayload)
  }
  
  const result = nextState(mockState)(mockUpdates)(actionType)
  t.deepEqual(mockState, result)
})

test('nextState returns the a new state when an update function is found', t => {
  const actionType = 'SET_TITLE'
  const mockPayload = { [TITLE]: 'New Subtitle' }
  const mockUpdates = {
    SET_TITLE: () => merge(mockState)(mockPayload)
  }
  
  const result = nextState(mockState)(mockUpdates)(actionType)
  t.notDeepEqual(mockState, result)
})

test('nextState produces the next state correctly', t => {
  t.plan(3)

  const actionType = 'SET_TITLE'
  const mockPayload = { [TITLE]: 'New Subtitle' }
  const mockUpdates = {
    SET_TITLE: () => merge(mockState)(mockPayload)
  }
  
  const result = nextState(mockState)(mockUpdates)(actionType)
  t.notDeepEqual(mockState[TITLE], mockPayload[TITLE])
  t.notDeepEqual(mockState[TITLE], result[TITLE])
  t.deepEqual(result[TITLE], mockPayload[TITLE])
})