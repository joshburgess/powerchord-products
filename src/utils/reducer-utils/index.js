// @flow
import merge from 'ramda/src/merge'
import {
  IS_PINGING,
  IS_FETCHING_PRODUCTS,
  IS_FETCHING_SCHEMA,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
} from '../../constants/stateKeys'
import prop from 'ramda/src/prop'

export const nextState = state => updates => type => {
  const fallback = () => state
  const update = prop(type)(updates)

  return (update || fallback)()
}

export const mergeIsPinging = state => boolVal =>
  merge(state)({
    [IS_PINGING]: boolVal || false,
  })

export const mergeSetIsFetchingProducts = state => payload =>
  merge(state)({
    [IS_FETCHING_PRODUCTS]: prop(IS_FETCHING_PRODUCTS)(payload) || false,
  })

export const mergeSetIsFetchingSchema = state => payload =>
  merge(state)({
    [IS_FETCHING_SCHEMA]: prop(IS_FETCHING_SCHEMA)(payload) || false,
  })

export const mergeReceiveProducts = state => payload =>
  merge(state)({
    [PRODUCTS]: prop(PRODUCTS)(payload) || {},
  })

export const mergeReceiveSchema = state => payload =>
  merge(state)({
    [SCHEMA]: prop(SCHEMA)(payload) || {},
  })

export const mergeSetProductsLoaded = state => payload =>
  merge(state)({
    [PRODUCTS_LOADED]: prop(PRODUCTS_LOADED)(payload) || false,
  })

export const mergeSetSchemaLoaded = state => payload =>
  merge(state)({
    [SCHEMA_LOADED]: prop(SCHEMA_LOADED)(payload) || false,
  })

export const mergeSetSelectedProduct = state => payload =>
  merge(state)({
    [SELECTED_PRODUCT]: prop(SELECTED_PRODUCT)(payload) || false,
  })
