// @flow
import {
  NumberPresentation,
  TextPresentation,
  CurrencyPresentation,
} from '../components/Presentations'
import StatusSelect from '../containers/StatusSelect'
import {
  NUMBER,
  TEXT,
  CURRENCY,
  SELECT,
} from '../constants/presentationTypes'
import prop from 'ramda/src/prop'

const PRESENTATIONS = ({
  [NUMBER]: NumberPresentation,
  [TEXT]: TextPresentation,
  [CURRENCY]: CurrencyPresentation,
  [SELECT]: StatusSelect,
})

export const getPresentation = presentation =>
  prop(presentation)(PRESENTATIONS) || TextPresentation
