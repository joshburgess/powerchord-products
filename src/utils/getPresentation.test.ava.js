import test from 'ava'
import {
  getPresentation,
} from './getPresentation'
import {
  NumberPresentation,
  TextPresentation,
  CurrencyPresentation,
  SelectPresentation,
} from '../components/Presentations'
import StatusSelect from '../containers/StatusSelect'
import {
  NUMBER,
  TEXT,
  CURRENCY,
  SELECT,
} from '../constants/presentationTypes'

/* eslint-disable */

test('getPresentation returns the StatusSelect container, not the SelectPresentation component, when passed "select"', t => {
  t.plan(2)
  
  const selectResult = getPresentation(SELECT)

  t.notDeepEqual(selectResult, SelectPresentation)
  t.deepEqual(selectResult, StatusSelect)
})

test('getPresentation retrieves the correct components when passed valid keys', t => {
  t.plan(4)
  
  const numberResult = getPresentation(NUMBER)
  const textResult = getPresentation(TEXT)
  const currencyResult = getPresentation(CURRENCY)
  const selectResult = getPresentation(SELECT)

  t.deepEqual(numberResult, NumberPresentation)
  t.deepEqual(textResult, TextPresentation)
  t.deepEqual(currencyResult, CurrencyPresentation)
  t.deepEqual(selectResult, StatusSelect)
})

test('getPresentation defaults to returning the TextPresentation when passed an invalid key', t => { 
  const selectResult = getPresentation('unknownType')

  t.deepEqual(selectResult, TextPresentation)
})