import test from 'ava'
import { unmountComponentAtNode } from 'react-dom'
import store from './store'
import mountApp from './utils/mountApp'
import configureStore from 'redux-mock-store'
import {
  IS_FETCHING_PRODUCTS,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
  TITLE,
} from './constants/stateKeys'

/* eslint-disable */

const mockStore = configureStore()
const initialState = {
  app: {
    [TITLE]: 'PowerChord Products Exercise',
    [IS_FETCHING_PRODUCTS]: false,
    [PRODUCTS]: [],
    [PRODUCTS_LOADED]: false,
    [SCHEMA]: [],
    [SCHEMA_LOADED]: false,
    [SELECTED_PRODUCT]: '',
  },
}

test('renders without crashing using a mock store', t => {
  const root = document.createElement('root')
  mountApp(mockStore(initialState))(root)
  unmountComponentAtNode(root)
  t.pass()
})

test('renders without crashing using the real store', t => {
  const root = document.createElement('root')
  mountApp(store)(root)
  unmountComponentAtNode(root)
  t.pass()
})

