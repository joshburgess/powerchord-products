import test from 'ava'
import {
  ping,
  pong,
  setIsFetchingProducts,
  setIsFetchingSchema,
  setSelectedProduct,
  fetchProducts,
  setProductsLoaded,
  fetchSchema,
  setSchemaLoaded,
  receiveProducts,
  receiveSchema,
} from './'
import {
  PING,
  PONG,
  FETCH_PRODUCTS,
  FETCH_SCHEMA,
  RECEIVE_PRODUCTS,
  RECEIVE_SCHEMA,
  SET_IS_FETCHING_PRODUCTS,
  SET_IS_FETCHING_SCHEMA,
  SET_PRODUCTS_LOADED,
  SET_SCHEMA_LOADED,
  SET_SELECTED_PRODUCT,
} from '../constants/actionTypes'
import {
  IS_FETCHING_PRODUCTS,
  IS_FETCHING_SCHEMA,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
} from '../constants/stateKeys'

/* eslint-disable */

test('ping properly creates a PING action', t => {
  const result = ping()
  const expected = {
    type: PING,
  }

  t.deepEqual(expected, result)
})

test('pong properly creates a PONG action', t => {
  const result = pong()
  const expected = {
    type: PONG,
  }

  t.deepEqual(expected, result)
})

test('setIsFetchingProducts properly creates a SET_IS_FETCHING_PRODUCTS action', t => {
  const result = setIsFetchingProducts(true)
  const expected = {
    type: SET_IS_FETCHING_PRODUCTS,
    payload: {
      [IS_FETCHING_PRODUCTS]: true,
    }
  }

  t.deepEqual(expected, result)
})

test('setIsFetchingSchema properly creates a SET_IS_FETCHING_SCHEMA action', t => {
  const result = setIsFetchingSchema(true)
  const expected = {
    type: SET_IS_FETCHING_SCHEMA,
    payload: {
      [IS_FETCHING_SCHEMA]: true,
    }
  }

  t.deepEqual(expected, result)
})

test('setSelectedProduct properly creates a SET_SELECTED_PRODUCT action', t => {
  const result = setSelectedProduct(1)
  const expected = {
    type: SET_SELECTED_PRODUCT,
    payload: {
      [SELECTED_PRODUCT]: 1,
    }
  }

  t.deepEqual(expected, result)
})

test('fetchProducts properly creates a FETCH_PRODUCTS action', t => {
  const result = fetchProducts()
  const expected = {
    type: FETCH_PRODUCTS,
  }

  t.deepEqual(expected, result)
})

test('setProductsLoaded properly creates a SET_PRODUCTS_LOADED action', t => {
  const result = setProductsLoaded(true)
  const expected = {
    type: SET_PRODUCTS_LOADED,
    payload: {
      [PRODUCTS_LOADED]: true,
    }
  }

  t.deepEqual(expected, result)
})

test('receiveProducts properly creates a RECEIVE_PRODUCTS action', t => {
  const mockProducts = [
    {
      ID: 1,
      status: 'scheduled',
      upc: 'BA776009705700629004',
      name: 'Handcrafted Rubber Pizza',
      price: '283.63',
      description:
        'Unde voluptatem nihil. Repellendus est ab. Rerum nam nemo dignissimos quia corporis expedita. Debitis omnis enim corporis et assumenda.',
    },
    {
      ID: 2,
      status: 'scheduled',
      upc: 'PT56099090506183082764604',
      name: 'Practical Wooden Chicken',
      price: '167.43',
      description:
        'Corrupti eaque fuga consequatur officia illum et rem excepturi. Sunt non et consequatur adipisci sit minima magnam saepe sunt. Aspernatur facere mollitia laboriosam deserunt reiciendis.',
    },
  ]
  const result = receiveProducts(mockProducts)
  const expected = {
    type: RECEIVE_PRODUCTS,
    payload: {
      [PRODUCTS]: {
        '1': {
          ID: 1,
          status: 'scheduled',
          upc: 'BA776009705700629004',
          name: 'Handcrafted Rubber Pizza',
          price: '283.63',
          description: 'Unde voluptatem nihil. Repellendus est ab. Rerum nam nemo dignissimos quia corporis expedita. Debitis omnis enim corporis et assumenda.'
        },
        '2': {
          ID: 2,
          status: 'scheduled',
          upc: 'PT56099090506183082764604',
          name: 'Practical Wooden Chicken',
          price: '167.43',
          description: 'Corrupti eaque fuga consequatur officia illum et rem excepturi. Sunt non et consequatur adipisci sit minima magnam saepe sunt. Aspernatur facere mollitia laboriosam deserunt reiciendis.'
        },
      },
    }
  }

  t.deepEqual(expected, result)
})

test('receiveSchema properly creates a RECEIVE_SCHEMA action', t => {
  const mockSchema = {
    resource: 'products',
    fields: [
      {
        label: 'ID',
        key: 'ID',
        position: 0,
        presentation: 'number',
      },
      {
        label: 'Status',
        key: 'status',
        position: 6,
        presentation: 'select',
      },
      {
        label: 'Name',
        key: 'name',
        position: 1,
        presentation: 'text',
      },
    ]
  }
  const result = receiveSchema(mockSchema)
  const expected = {
    type: RECEIVE_SCHEMA,
    payload: {
      [SCHEMA]: {
        ID: {
          label: 'ID',
          key: 'ID',
          position: 0,
          presentation: 'number',
        },
        status: {
          label: 'Status',
          key: 'status',
          position: 6,
          presentation: 'select',
        },
        name: {
          label: 'Name',
          key: 'name',
          position: 1,
          presentation: 'text',
        },
      },
    }
  }

  t.deepEqual(expected, result)
})