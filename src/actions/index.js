// @flow
import {
  PING,
  PONG,
  FETCH_PRODUCTS,
  FETCH_SCHEMA,
  RECEIVE_PRODUCTS,
  RECEIVE_SCHEMA,
  SET_IS_FETCHING_PRODUCTS,
  SET_IS_FETCHING_SCHEMA,
  SET_PRODUCTS_LOADED,
  SET_SCHEMA_LOADED,
  SET_SELECTED_PRODUCT,
} from '../constants/actionTypes'
import {
  IS_FETCHING_PRODUCTS,
  IS_FETCHING_SCHEMA,
  // IS_PINGING,
  PRODUCTS,
  PRODUCTS_LOADED,
  SCHEMA,
  SCHEMA_LOADED,
  SELECTED_PRODUCT,
} from '../constants/stateKeys'
import { ID } from '../constants/schemaKeys'
import { KEY } from '../constants/schemaProps'
import compose from 'ramda/src/compose'
import map from 'ramda/src/map'
import mergeAll from 'ramda/src/mergeAll'
import prop from 'ramda/src/prop'

export const ping = () => ({
  type: PING,
})

export const pong = () => ({
  type: PONG,
})

export const setIsFetchingProducts = isFetchingProducts => ({
  type: SET_IS_FETCHING_PRODUCTS,
  payload: {
    [IS_FETCHING_PRODUCTS]: isFetchingProducts,
  },
})

export const setIsFetchingSchema = isFetchingSchema => ({
  type: SET_IS_FETCHING_SCHEMA,
  payload: {
    [IS_FETCHING_SCHEMA]: isFetchingSchema,
  },
})

export const setSelectedProduct = selectedProduct => ({
  type: SET_SELECTED_PRODUCT,
  payload: {
    [SELECTED_PRODUCT]: selectedProduct,
  },
})

export const fetchProducts = () => ({
  type: FETCH_PRODUCTS,
})

export const setProductsLoaded = productsLoaded => ({
  type: SET_PRODUCTS_LOADED,
  payload: {
    [PRODUCTS_LOADED]: productsLoaded,
  },
})

export const fetchSchema = () => ({
  type: FETCH_SCHEMA,
})

export const setSchemaLoaded = schemaLoaded => ({
  type: SET_SCHEMA_LOADED,
  payload: {
    [SCHEMA_LOADED]: schemaLoaded,
  },
})

export const receiveProducts = products => {
  // index by product ID
  const toIndexedObj = product => ({ [prop(ID)(product)]: { ...product } })
  const indexedProducts = compose(
    mergeAll,
    map(toIndexedObj)
  )(products)

  return {
    type: RECEIVE_PRODUCTS,
    payload: {
      [PRODUCTS]: indexedProducts,
    },
  }
}

export const receiveSchema = schema => {
  const { fields } = schema

  // index by field key
  const toIndexedObj = field => ({ [prop(KEY)(field)]: { ...field } })
  const indexedProductFields = compose(
    mergeAll,
    map(toIndexedObj)
  )(fields)

  return {
    type: RECEIVE_SCHEMA,
    payload: {
      [SCHEMA]: indexedProductFields,
    },
  }
}
